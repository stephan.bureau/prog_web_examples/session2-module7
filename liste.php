<html>

<head>
  <base href="/" />
  <title>Ceci est mon titre</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
  <?php
  include_once("./bd.php");
  $bd = new BD();
  ?>
  <p><a href="./index.php">Revenir la page d'accueil</a></p>

  <label>Noms des contacts:</label>
  <ul>
    <?php
    // $stmt = $bd->pdo->query('SELECT name FROM contact');
    // while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
    //   echo "<li>" . $row->name . "</li>";
    // }
    $fetch = $bd->getContactsList();
    foreach ($fetch as $key => $row) {
      echo "<li>" . $row->name . "</li>";
    }
    ?>
  </ul>

</body>

</html>