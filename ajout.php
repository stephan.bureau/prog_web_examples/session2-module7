<html>

<head>
  <base href="/" />
  <title>Ceci est mon titre</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
  <?php
  include_once("./bd.php");
  $bd = new BD();

  // si les variables de formulaire "name" et "address"
  if (isset($_POST["name"]) && isset($_POST["address"])) {
    $success = false;
    try {
      $bd->ajoutContact($_POST["name"], $_POST["address"]);
      $success = true;
    } catch (MissingFieldException $mfe) {
      echo "Erreur avec une colonne dans la base de données";
    }

    // si la commande n'a pas eu d'erreur
    if ($success) {
      // on affiche le message de succès pour avoir créé un nouveau contact
      echo "<h2>Nouveau contact créé avec succès!</h2>";
    } else {
      echo "<h2>Erreur en essayant de créer le contact</h2>";
    }
  }
  ?>
  <p><a href="./index.php">Revenir la page d'accueil</a></p>
  <br>

  <!-- <h2 id="form-message"></h2>
  <i>Nom et adresse du nouveau contact :</i>
  <form action="./ajout.php" method="POST">
    <input id="form-name" name="name" type="text" placeholder="Nom du contact" />
    <input id="form-address" name="address" type="text" placeholder="Adresse du contact" />
    <input type="submit" value="Créer">
  </form> -->

  <h2 id="form-message"></h2>
  <i>Nom et adresse du nouveau contact :</i>
  <form onsubmit="ajoutAjax(); return false" method="POST">
    <input id="form-name" name="name" type="text" placeholder="Nom du contact" />
    <input id="form-address" name="address" type="text" placeholder="Adresse du contact" />
    <input type="submit" value="Créer">
  </form>


  <script>
    async function ajoutAjax() {
      // console.log("ceci est un test");
      if (window.fetch) {
        const name = document.querySelector("#form-name").value;
        const address = document.querySelector("#form-address").value;
        const body = new URLSearchParams(`name=${name}&address=${address}`);
        try {
          const response = await fetch("/ajoutAjax.php", {
            method: "POST",
            body: body
          });
          if (response.ok) {
            const data = await response.json();
            console.log(data.result)
            document.querySelector("#form-message").innerHTML = "Nouveau contact créé avec succès!";
          }
        } catch (err) {
          console.error(err);
        }
      } else {
        console.log("fetch is not supported by your browser");
      }
    }
  </script>
</body>

</html>